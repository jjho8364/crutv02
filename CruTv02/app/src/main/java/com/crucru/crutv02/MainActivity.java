    package com.crucru.crutv02;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.ContactsContract;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends Activity {
    private final String TAG = "MainActivity : ";

    private WebView webView =null;
    private FrameLayout customViewContainer;
    private WebChromeClient.CustomViewCallback customViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;

    private InterstitialAd interstitialAd;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String model = Build.MODEL.toLowerCase();
        TelephonyManager telephony = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telephony.getNetworkOperator();
        int portrait_width_pixel=Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        boolean phoneNumFlag = false;
        boolean operatorFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        //Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        //int contactsCnt = c.getCount();
        //if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
        //String phoneNum = telManager.getLine1Number();
        //if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        if(operator == null || operator.equals("")){ operatorFlag = true; }
        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "operatorFlag : " + operatorFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if ( (model.equals("sph-d720") || model.contains("nexus") || operatorFlag) && isPhone) {
            setContentView(R.layout.depend);
        } else {
            setContentView(R.layout.activity_main);
            Intent intent = getIntent();
            String urlPath = (String)intent.getSerializableExtra("urlPath");

            AdView mAdViewUpper = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdViewUpper.loadAd(adRequest);

            interstitialAd = new InterstitialAd(this);
            interstitialAd.setAdUnitId("ca-app-pub-7729076286224738/1046496401");
            interstitialAd.loadAd(adRequest);

            customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
            webView = (WebView) findViewById(R.id.webView);

            mWebViewClient = new myWebViewClient();
            webView.setWebViewClient(mWebViewClient);
        /*String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
        webView.getSettings().setUserAgentString(newUA);*/
            mWebChromeClient = new myWebChromeClient();
            webView.setWebChromeClient(mWebChromeClient);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setAppCacheEnabled(true);
            webView.getSettings().setSaveFormData(true);

            //Toast.makeText(MainActivity.this, "화면이 떨리면서 멈출경우 전체화면 버튼을 누르세요.", Toast.LENGTH_SHORT).show();
            webView.loadUrl("http://www.koreaba.com/");
        }




    }

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    public void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();    //To change body of overridden methods use File | Settings | File Templates.
        webView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();    //To change body of overridden methods use File | Settings | File Templates.
        webView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();    //To change body of overridden methods use File | Settings | File Templates.
        if (inCustomView()) {
            hideCustomView();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view,CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            webView.setVisibility(View.GONE);
            customViewContainer.setVisibility(View.VISIBLE);
            customViewContainer.addView(view);
            customViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            webView.setVisibility(View.VISIBLE);
            customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(interstitialAd.isLoaded()){
            interstitialAd.show();
        }
    }
}
